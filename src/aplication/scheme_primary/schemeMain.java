package aplication.scheme_primary;

import aplication.login.login;
import com.formdev.flatlaf.FlatLightLaf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import connection.model.user;
import java.awt.Image;
import java.awt.Toolkit;
import connection.connection;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;


public class schemeMain extends javax.swing.JFrame {
  
  private Image data;
  
  connection con = new connection();
  user usr;
  public schemeMain() {
    initComponents();
  }
  
  public schemeMain(user usr) {
    this.setUndecorated(false);
    initComponents();
    this.setLocationRelativeTo(null);
    this.setTitle("Yaty Book 1.0");
    this.setResizable(false);
    this.setSize(1083, 740);
    setIconImage(getIconImage());
    imgPerfil.setIcon(getPerfilUser(usr.getId_teacher()));
    imgPerfil.setSize(45,45);
    this.usr = usr;
    lblNameUser.setText(usr.getName_teacher());
    if(usr.getId_category() == 1){
      btnRegisterUsers.setVisible(true);
      lblTypeUser.setText("ADMINISTRADOR");
    } else {
      btnRegisterUsers.setVisible(false);
      lblTypeUser.setText("DOCENTE");
    }
  }
  
  public ImageIcon getPerfilUser(String code) {
    Image dtFlux = getImageUser(code);
    ImageIcon icon = new ImageIcon(dtFlux);
    Image img = icon.getImage();
    Image newImg = img.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH);
    ImageIcon newIcon = new ImageIcon(newImg);
    return newIcon;
  }
  
  public Image getImageUser(String code) {
    try {
      String sql = "SELECT image_teacher FROM teacher WHERE id_teacher=(?);";
      PreparedStatement ps = con.getConnection().prepareStatement(sql);
      ps.setString(1, code);
      ResultSet rs = ps.executeQuery();
      int index = 0;
      while (rs.next()) {
        byte[] support = rs.getBytes("image_teacher");
        data = getByteImage(support);
        index++;
      }
      rs.close();
    } catch (IOException ex) {
          Logger.getLogger(schemeMain.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex){
      System.out.println(ex);
    }
    con.desconnection();
    return data;
  }
  
  private Image getByteImage(byte[] bytes) throws IOException{
    ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
    Iterator readers = ImageIO.getImageReadersByFormatName("jpeg");
    ImageReader reader = (ImageReader) readers.next();
    Object source = bis;
    ImageInputStream iis = ImageIO.createImageInputStream(source);
    reader.setInput(iis, true);
    ImageReadParam param = reader.getDefaultReadParam();
    return reader.read(0,param);
  }
  @Override
  public Image getIconImage(){
    Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icons/Icon-YB.png"));
    return retValue;
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelRound1 = new schemes.PanelRound();
    panelImage1 = new org.edisoncor.gui.panel.PanelImage();
    jSeparator1 = new javax.swing.JSeparator();
    jSeparator2 = new javax.swing.JSeparator();
    lblNameUser = new javax.swing.JLabel();
    lblTypeUser = new javax.swing.JLabel();
    panelRound2 = new schemes.PanelRound();
    btnOptionUser = new javax.swing.JButton();
    btnClose = new javax.swing.JButton();
    btnBooks = new javax.swing.JButton();
    btnHome = new javax.swing.JButton();
    btnRegisterBooks = new javax.swing.JButton();
    btnRegisterUsers = new javax.swing.JButton();
    imgPerfil = new javax.swing.JLabel();
    JPanePromise = new javax.swing.JPanel();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setBackground(new java.awt.Color(240, 241, 238));
    setMinimumSize(new java.awt.Dimension(1083, 740));
    setUndecorated(true);
    getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panelRound1.setBackground(new java.awt.Color(47, 64, 79));
    panelRound1.setRoundBottomLeft(15);
    panelRound1.setRoundBottomRight(15);
    panelRound1.setRoundTopLeft(15);
    panelRound1.setRoundTopRight(15);
    panelRound1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/Icon_menu_YB.png"))); // NOI18N

    javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
    panelImage1.setLayout(panelImage1Layout);
    panelImage1Layout.setHorizontalGroup(
      panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 140, Short.MAX_VALUE)
    );
    panelImage1Layout.setVerticalGroup(
      panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 35, Short.MAX_VALUE)
    );

    panelRound1.add(panelImage1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, -1, -1));

    jSeparator1.setForeground(new java.awt.Color(255, 255, 255));
    panelRound1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 75, 210, -1));

    jSeparator2.setForeground(new java.awt.Color(255, 255, 255));
    panelRound1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 144, 210, -1));

    lblNameUser.setBackground(new java.awt.Color(255, 255, 255));
    lblNameUser.setFont(new java.awt.Font("Segoe UI", 1, 13)); // NOI18N
    lblNameUser.setForeground(new java.awt.Color(255, 255, 255));
    lblNameUser.setText("name_User");
    panelRound1.add(lblNameUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(82, 90, -1, -1));

    lblTypeUser.setBackground(new java.awt.Color(204, 204, 204));
    lblTypeUser.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
    lblTypeUser.setForeground(new java.awt.Color(204, 204, 204));
    lblTypeUser.setText("ACTIVITY");
    panelRound1.add(lblTypeUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(82, 114, -1, -1));

    panelRound2.setBackground(new java.awt.Color(56, 148, 161));
    panelRound2.setRoundBottomLeft(10);
    panelRound2.setRoundBottomRight(10);
    panelRound2.setRoundTopLeft(10);
    panelRound2.setRoundTopRight(10);

    btnOptionUser.setBackground(new java.awt.Color(56, 148, 161));
    btnOptionUser.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    btnOptionUser.setForeground(new java.awt.Color(255, 255, 255));
    btnOptionUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icon_user.png"))); // NOI18N
    btnOptionUser.setText("Usuario");
    btnOptionUser.setBorder(null);
    btnOptionUser.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    btnOptionUser.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnOptionUserActionPerformed(evt);
      }
    });

    btnClose.setBackground(new java.awt.Color(56, 148, 161));
    btnClose.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    btnClose.setForeground(new java.awt.Color(255, 255, 255));
    btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icon_close.png"))); // NOI18N
    btnClose.setText("Cerrar Sesion");
    btnClose.setBorder(null);
    btnClose.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    btnClose.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnCloseActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout panelRound2Layout = new javax.swing.GroupLayout(panelRound2);
    panelRound2.setLayout(panelRound2Layout);
    panelRound2Layout.setHorizontalGroup(
      panelRound2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(panelRound2Layout.createSequentialGroup()
        .addGap(14, 14, 14)
        .addGroup(panelRound2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(btnOptionUser, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(16, Short.MAX_VALUE))
    );
    panelRound2Layout.setVerticalGroup(
      panelRound2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(panelRound2Layout.createSequentialGroup()
        .addGap(18, 18, 18)
        .addComponent(btnOptionUser, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(26, Short.MAX_VALUE))
    );

    panelRound1.add(panelRound2, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 540, 220, 130));

    btnBooks.setBackground(new java.awt.Color(47, 64, 79));
    btnBooks.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    btnBooks.setForeground(new java.awt.Color(255, 255, 255));
    btnBooks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icon_book.png"))); // NOI18N
    btnBooks.setText("Libros");
    btnBooks.setBorder(null);
    btnBooks.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    btnBooks.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnBooksActionPerformed(evt);
      }
    });
    panelRound1.add(btnBooks, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 190, 40));

    btnHome.setBackground(new java.awt.Color(47, 64, 79));
    btnHome.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    btnHome.setForeground(new java.awt.Color(255, 255, 255));
    btnHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icon_home.png"))); // NOI18N
    btnHome.setText("Inicio");
    btnHome.setBorder(null);
    btnHome.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    panelRound1.add(btnHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 190, 40));

    btnRegisterBooks.setBackground(new java.awt.Color(47, 64, 79));
    btnRegisterBooks.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    btnRegisterBooks.setForeground(new java.awt.Color(255, 255, 255));
    btnRegisterBooks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icon_bookmark.png"))); // NOI18N
    btnRegisterBooks.setText("Registro de Libros");
    btnRegisterBooks.setBorder(null);
    btnRegisterBooks.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    btnRegisterBooks.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnRegisterBooksActionPerformed(evt);
      }
    });
    panelRound1.add(btnRegisterBooks, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 190, 40));

    btnRegisterUsers.setBackground(new java.awt.Color(47, 64, 79));
    btnRegisterUsers.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    btnRegisterUsers.setForeground(new java.awt.Color(255, 255, 255));
    btnRegisterUsers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icon_users.png"))); // NOI18N
    btnRegisterUsers.setText("Registro de Usuarios");
    btnRegisterUsers.setBorder(null);
    btnRegisterUsers.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    btnRegisterUsers.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnRegisterUsersActionPerformed(evt);
      }
    });
    panelRound1.add(btnRegisterUsers, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 190, 40));

    imgPerfil.setBackground(new java.awt.Color(255, 255, 255));
    imgPerfil.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(56, 148, 161), 3, true));
    imgPerfil.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    panelRound1.add(imgPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 45, 45));

    getContentPane().add(panelRound1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 230, 700));

    javax.swing.GroupLayout JPanePromiseLayout = new javax.swing.GroupLayout(JPanePromise);
    JPanePromise.setLayout(JPanePromiseLayout);
    JPanePromiseLayout.setHorizontalGroup(
      JPanePromiseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 807, Short.MAX_VALUE)
    );
    JPanePromiseLayout.setVerticalGroup(
      JPanePromiseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 700, Short.MAX_VALUE)
    );

    getContentPane().add(JPanePromise, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, -1, 700));

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void btnOptionUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOptionUserActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnOptionUserActionPerformed

  private void btnRegisterBooksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterBooksActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnRegisterBooksActionPerformed

  private void btnRegisterUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterUsersActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnRegisterUsersActionPerformed

  private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
    login log = new login();
    log.setVisible(true);
    this.setVisible(false);
  }//GEN-LAST:event_btnCloseActionPerformed

  private void btnBooksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBooksActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnBooksActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    try {
      FlatLightLaf.setup();
    } catch (Exception e) {
      e.printStackTrace();
    }

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new schemeMain().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel JPanePromise;
  private javax.swing.JButton btnBooks;
  private javax.swing.JButton btnClose;
  private javax.swing.JButton btnHome;
  private javax.swing.JButton btnOptionUser;
  private javax.swing.JButton btnRegisterBooks;
  private javax.swing.JButton btnRegisterUsers;
  private javax.swing.JLabel imgPerfil;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JLabel lblNameUser;
  private javax.swing.JLabel lblTypeUser;
  private org.edisoncor.gui.panel.PanelImage panelImage1;
  private schemes.PanelRound panelRound1;
  private schemes.PanelRound panelRound2;
  // End of variables declaration//GEN-END:variables
}
