package aplication.scheme_primary.features.books;

public class books extends javax.swing.JPanel {

  public books() {
    initComponents();
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel1 = new javax.swing.JLabel();
    panelRound1 = new schemes.PanelRound();
    btnSolicited = new javax.swing.JButton();
    jSeparator1 = new javax.swing.JSeparator();
    btnView = new javax.swing.JButton();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    jLabel6 = new javax.swing.JLabel();
    btnAdd = new javax.swing.JButton();
    lblSelectStock = new javax.swing.JLabel();
    lblSelectISBN = new javax.swing.JLabel();
    lblSelectName = new javax.swing.JLabel();
    lblSelectAutor = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    jTexfieldPH_FielTex1 = new LIB.JTexfieldPH_FielTex();
    btnSearch = new javax.swing.JButton();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblBooks = new javax.swing.JTable();

    setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(47, 64, 79));
    jLabel1.setText("Tablero de Libros");
    add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 12, -1, -1));

    panelRound1.setBackground(new java.awt.Color(255, 255, 255));
    panelRound1.setRoundBottomLeft(10);
    panelRound1.setRoundBottomRight(10);
    panelRound1.setRoundTopLeft(10);
    panelRound1.setRoundTopRight(10);
    panelRound1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    btnSolicited.setBackground(new java.awt.Color(56, 148, 161));
    btnSolicited.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
    btnSolicited.setForeground(new java.awt.Color(255, 255, 255));
    btnSolicited.setText("Solicitar");
    btnSolicited.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnSolicitedActionPerformed(evt);
      }
    });
    panelRound1.add(btnSolicited, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 40, 172, 32));

    jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
    panelRound1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(491, 0, 10, 210));

    btnView.setBackground(new java.awt.Color(47, 64, 79));
    btnView.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
    btnView.setForeground(new java.awt.Color(255, 255, 255));
    btnView.setText("Visualizar");
    btnView.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnViewActionPerformed(evt);
      }
    });
    panelRound1.add(btnView, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 90, 172, 32));

    jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
    jLabel2.setForeground(new java.awt.Color(56, 148, 161));
    jLabel2.setText("Datos del Libro");
    panelRound1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

    jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    jLabel3.setForeground(new java.awt.Color(51, 51, 51));
    jLabel3.setText("ISBN:");
    panelRound1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, -1, -1));

    jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    jLabel4.setForeground(new java.awt.Color(51, 51, 51));
    jLabel4.setText("Nombre:");
    panelRound1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, -1));

    jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    jLabel5.setForeground(new java.awt.Color(51, 51, 51));
    jLabel5.setText("Autor:");
    panelRound1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, -1));

    jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    jLabel6.setForeground(new java.awt.Color(51, 51, 51));
    jLabel6.setText("Stock:");
    panelRound1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, -1));

    btnAdd.setBackground(new java.awt.Color(0, 0, 102));
    btnAdd.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
    btnAdd.setForeground(new java.awt.Color(255, 255, 255));
    btnAdd.setText("Agregar");
    btnAdd.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnAddActionPerformed(evt);
      }
    });
    panelRound1.add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 140, 172, 32));

    lblSelectStock.setForeground(new java.awt.Color(51, 51, 51));
    lblSelectStock.setText("Sin Seleccionar");
    panelRound1.add(lblSelectStock, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 360, -1));

    lblSelectISBN.setForeground(new java.awt.Color(51, 51, 51));
    lblSelectISBN.setText("Sin Seleccionar");
    panelRound1.add(lblSelectISBN, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 50, 330, -1));

    lblSelectName.setForeground(new java.awt.Color(51, 51, 51));
    lblSelectName.setText("Sin Seleccionar");
    panelRound1.add(lblSelectName, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 90, 340, -1));

    lblSelectAutor.setForeground(new java.awt.Color(51, 51, 51));
    lblSelectAutor.setText("Sin Seleccionar");
    panelRound1.add(lblSelectAutor, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 130, 340, -1));

    add(panelRound1, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 56, 770, -1));

    jLabel7.setText("Buscar:");
    add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, -1, 20));

    jTexfieldPH_FielTex1.setPlaceholder("Buscar Libro");
    add(jTexfieldPH_FielTex1, new org.netbeans.lib.awtextra.AbsoluteConstraints(91, 284, 590, -1));

    btnSearch.setBackground(new java.awt.Color(56, 148, 161));
    btnSearch.setForeground(new java.awt.Color(255, 255, 255));
    btnSearch.setText("Buscar");
    add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 285, 90, 30));

    tblBooks.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "ISBN", "Nombre de Libro", "Autor", "Stock"
      }
    ) {
      boolean[] canEdit = new boolean [] {
        false, false, false, false
      };

      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit [columnIndex];
      }
    });
    jScrollPane1.setViewportView(tblBooks);

    add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 770, 360));
  }// </editor-fold>//GEN-END:initComponents

  private void btnSolicitedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSolicitedActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnSolicitedActionPerformed

  private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnViewActionPerformed

  private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnAddActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnAdd;
  private javax.swing.JButton btnSearch;
  private javax.swing.JButton btnSolicited;
  private javax.swing.JButton btnView;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JSeparator jSeparator1;
  private LIB.JTexfieldPH_FielTex jTexfieldPH_FielTex1;
  private javax.swing.JLabel lblSelectAutor;
  private javax.swing.JLabel lblSelectISBN;
  private javax.swing.JLabel lblSelectName;
  private javax.swing.JLabel lblSelectStock;
  private schemes.PanelRound panelRound1;
  private javax.swing.JTable tblBooks;
  // End of variables declaration//GEN-END:variables
}
