package aplication.scheme_primary.features.registerBooks;

public class registerBooks extends javax.swing.JPanel {

  public registerBooks() {
    initComponents();
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jTexfieldPH_FielTex1 = new LIB.JTexfieldPH_FielTex();
    jButton1 = new javax.swing.JButton();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblSolicitedData = new javax.swing.JTable();

    setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(47, 64, 79));
    jLabel1.setText("Registro de Libros");
    add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

    jLabel2.setText("Buscar:");
    add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, -1, -1));

    jTexfieldPH_FielTex1.setPlaceholder("Buscar la Solicitud");
    add(jTexfieldPH_FielTex1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 70, 610, -1));

    jButton1.setBackground(new java.awt.Color(56, 148, 161));
    jButton1.setForeground(new java.awt.Color(255, 255, 255));
    jButton1.setText("Buscar");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });
    add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 70, 70, 30));

    tblSolicitedData.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null, null, null},
        {null, null, null, null, null, null},
        {null, null, null, null, null, null},
        {null, null, null, null, null, null}
      },
      new String [] {
        "ISBN", "Nombre del Libro", "Docente", "Cantidad", "Fecha de Solcitud", "Fecha de Retorno"
      }
    ) {
      boolean[] canEdit = new boolean [] {
        false, false, false, false, false, false
      };

      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit [columnIndex];
      }
    });
    jScrollPane1.setViewportView(tblSolicitedData);

    add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 750, 540));
  }// </editor-fold>//GEN-END:initComponents

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_jButton1ActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton jButton1;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JScrollPane jScrollPane1;
  private LIB.JTexfieldPH_FielTex jTexfieldPH_FielTex1;
  private javax.swing.JTable tblSolicitedData;
  // End of variables declaration//GEN-END:variables
}
