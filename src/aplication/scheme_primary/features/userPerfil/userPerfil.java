package aplication.scheme_primary.features.userPerfil;

public class userPerfil extends javax.swing.JPanel {

  public userPerfil() {
    initComponents();
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelRound1 = new schemes.PanelRound();
    imgUser = new javax.swing.JLabel();
    lblCurseUser = new javax.swing.JLabel();
    lblLevelUser = new javax.swing.JLabel();
    lblNameUser1 = new javax.swing.JLabel();
    lblRolUser = new javax.swing.JLabel();
    btnEditUser = new javax.swing.JButton();
    panelRound2 = new schemes.PanelRound();
    imgSchool = new javax.swing.JLabel();
    lblNameSchool = new javax.swing.JLabel();
    btnEditSchool = new javax.swing.JButton();
    jLabel1 = new javax.swing.JLabel();
    panelRound3 = new schemes.PanelRound();
    lbl = new javax.swing.JLabel();
    btnGit = new javax.swing.JButton();
    lbl1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    lbl2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();

    setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panelRound1.setBackground(new java.awt.Color(56, 148, 161));
    panelRound1.setRoundBottomLeft(10);
    panelRound1.setRoundBottomRight(10);
    panelRound1.setRoundTopLeft(10);
    panelRound1.setRoundTopRight(10);
    panelRound1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    imgUser.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(47, 64, 79), 5));
    panelRound1.add(imgUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(56, 47, 215, 215));

    lblCurseUser.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
    lblCurseUser.setForeground(new java.awt.Color(255, 255, 255));
    lblCurseUser.setText("curse_user");
    lblCurseUser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    panelRound1.add(lblCurseUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 402, 136, -1));

    lblLevelUser.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
    lblLevelUser.setForeground(new java.awt.Color(255, 255, 255));
    lblLevelUser.setText("nivel_user");
    lblLevelUser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    panelRound1.add(lblLevelUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 439, 136, -1));

    lblNameUser1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
    lblNameUser1.setForeground(new java.awt.Color(255, 255, 255));
    lblNameUser1.setText("name_user");
    lblNameUser1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    panelRound1.add(lblNameUser1, new org.netbeans.lib.awtextra.AbsoluteConstraints(56, 293, 215, -1));

    lblRolUser.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
    lblRolUser.setForeground(new java.awt.Color(47, 64, 79));
    lblRolUser.setText("rol_user");
    lblRolUser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    panelRound1.add(lblRolUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 324, 136, -1));

    btnEditUser.setBackground(new java.awt.Color(47, 64, 79));
    btnEditUser.setForeground(new java.awt.Color(255, 255, 255));
    btnEditUser.setText("Editar Datos");
    btnEditUser.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnEditUserActionPerformed(evt);
      }
    });
    panelRound1.add(btnEditUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(56, 527, 215, 40));

    add(panelRound1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 60, 330, 620));

    panelRound2.setBackground(new java.awt.Color(47, 64, 79));
    panelRound2.setRoundBottomLeft(10);
    panelRound2.setRoundBottomRight(10);
    panelRound2.setRoundTopLeft(10);
    panelRound2.setRoundTopRight(10);
    panelRound2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    imgSchool.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
    panelRound2.add(imgSchool, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 49, 290, 192));

    lblNameSchool.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
    lblNameSchool.setForeground(new java.awt.Color(255, 255, 255));
    lblNameSchool.setText("name_school");
    lblNameSchool.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    panelRound2.add(lblNameSchool, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 290, -1));

    btnEditSchool.setBackground(new java.awt.Color(56, 148, 161));
    btnEditSchool.setForeground(new java.awt.Color(255, 255, 255));
    btnEditSchool.setText("Editar Datos");
    btnEditSchool.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnEditSchoolActionPerformed(evt);
      }
    });
    panelRound2.add(btnEditSchool, new org.netbeans.lib.awtextra.AbsoluteConstraints(215, 350, 110, 40));

    add(panelRound2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 60, 350, 410));

    jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(47, 64, 79));
    jLabel1.setText("Datos de Usuario");
    add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, -1, -1));

    panelRound3.setBackground(new java.awt.Color(199, 218, 211));
    panelRound3.setRoundBottomLeft(10);
    panelRound3.setRoundBottomRight(10);
    panelRound3.setRoundTopLeft(10);
    panelRound3.setRoundTopRight(10);

    lbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    lbl.setForeground(new java.awt.Color(47, 64, 79));
    lbl.setText("Linea de Ayuda");
    lbl.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

    btnGit.setBackground(new java.awt.Color(153, 51, 0));
    btnGit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icon_gitlab.png"))); // NOI18N
    btnGit.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnGitActionPerformed(evt);
      }
    });

    lbl1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    lbl1.setForeground(new java.awt.Color(47, 64, 79));
    lbl1.setText("Versión:");
    lbl1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

    jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    jLabel2.setForeground(new java.awt.Color(0, 0, 51));
    jLabel2.setText("0.1");

    lbl2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    lbl2.setForeground(new java.awt.Color(47, 64, 79));
    lbl2.setText("Sofware:");
    lbl2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

    jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    jLabel3.setForeground(new java.awt.Color(204, 204, 0));
    jLabel3.setText("Premium");

    javax.swing.GroupLayout panelRound3Layout = new javax.swing.GroupLayout(panelRound3);
    panelRound3.setLayout(panelRound3Layout);
    panelRound3Layout.setHorizontalGroup(
      panelRound3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(panelRound3Layout.createSequentialGroup()
        .addGap(31, 31, 31)
        .addGroup(panelRound3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(panelRound3Layout.createSequentialGroup()
            .addGroup(panelRound3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
              .addGroup(panelRound3Layout.createSequentialGroup()
                .addComponent(lbl1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2))
              .addComponent(btnGit, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(74, 74, 74)
            .addComponent(lbl2)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jLabel3))
          .addComponent(lbl, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(48, Short.MAX_VALUE))
    );
    panelRound3Layout.setVerticalGroup(
      panelRound3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(panelRound3Layout.createSequentialGroup()
        .addGap(23, 23, 23)
        .addComponent(lbl)
        .addGap(18, 18, 18)
        .addComponent(btnGit, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
        .addGroup(panelRound3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(lbl1)
          .addComponent(jLabel2)
          .addComponent(lbl2)
          .addComponent(jLabel3))
        .addGap(20, 20, 20))
    );

    add(panelRound3, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 490, 350, 190));
  }// </editor-fold>//GEN-END:initComponents

  private void btnEditSchoolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditSchoolActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnEditSchoolActionPerformed

  private void btnEditUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditUserActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnEditUserActionPerformed

  private void btnGitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGitActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnGitActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnEditSchool;
  private javax.swing.JButton btnEditUser;
  private javax.swing.JButton btnGit;
  private javax.swing.JLabel imgSchool;
  private javax.swing.JLabel imgUser;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel lbl;
  private javax.swing.JLabel lbl1;
  private javax.swing.JLabel lbl2;
  private javax.swing.JLabel lblCurseUser;
  private javax.swing.JLabel lblLevelUser;
  private javax.swing.JLabel lblNameSchool;
  private javax.swing.JLabel lblNameUser1;
  private javax.swing.JLabel lblRolUser;
  private schemes.PanelRound panelRound1;
  private schemes.PanelRound panelRound2;
  private schemes.PanelRound panelRound3;
  // End of variables declaration//GEN-END:variables
}
