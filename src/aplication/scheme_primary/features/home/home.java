package aplication.scheme_primary.features.home;

public class home extends javax.swing.JPanel {

  public home() {
    initComponents();
  }
  
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel1 = new javax.swing.JLabel();
    lblNameUser = new javax.swing.JLabel();
    jSeparator1 = new javax.swing.JSeparator();
    btnSearch = new javax.swing.JButton();
    txtSearch = new LIB.JTexfieldPH_FielTex();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblBookUser = new javax.swing.JTable();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    lblNumber = new javax.swing.JLabel();
    panelRound1 = new schemes.PanelRound();
    jLabel5 = new javax.swing.JLabel();
    lblNameInstitution = new javax.swing.JLabel();
    lblDescrition = new javax.swing.JLabel();

    setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(47, 64, 79));
    jLabel1.setText("Bienvenido,");
    add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 22, -1, -1));

    lblNameUser.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
    lblNameUser.setForeground(new java.awt.Color(47, 64, 79));
    lblNameUser.setText("user Name");
    add(lblNameUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(162, 22, -1, -1));
    add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 66, 770, 10));

    btnSearch.setBackground(new java.awt.Color(56, 148, 161));
    btnSearch.setForeground(new java.awt.Color(255, 255, 255));
    btnSearch.setText("Buscar");
    btnSearch.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnSearchActionPerformed(evt);
      }
    });
    add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 150, -1, 30));

    txtSearch.setPlaceholder("Buscar Registro");
    add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 151, 420, 30));

    tblBookUser.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "ISBN", "Nombre del Libro", "Cantidad", "Estado"
      }
    ) {
      boolean[] canEdit = new boolean [] {
        false, false, false, false
      };

      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit [columnIndex];
      }
    });
    jScrollPane1.setViewportView(tblBookUser);

    add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 209, 490, 392));

    jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    jLabel2.setText("Buscar:");
    add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 119, -1, -1));

    jLabel3.setText("Total de Odenes: ");
    add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 90, -1, -1));

    lblNumber.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    lblNumber.setForeground(new java.awt.Color(47, 64, 79));
    lblNumber.setText("num");
    lblNumber.setMaximumSize(new java.awt.Dimension(25, 20));
    lblNumber.setMinimumSize(new java.awt.Dimension(25, 20));
    lblNumber.setPreferredSize(new java.awt.Dimension(25, 20));
    add(lblNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 30, -1));

    panelRound1.setBackground(new java.awt.Color(47, 64, 79));
    panelRound1.setRoundBottomLeft(8);
    panelRound1.setRoundBottomRight(8);
    panelRound1.setRoundTopLeft(8);
    panelRound1.setRoundTopRight(8);
    panelRound1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(56, 148, 161), 3));
    panelRound1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 23, 235, 160));

    lblNameInstitution.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
    lblNameInstitution.setForeground(new java.awt.Color(255, 255, 255));
    lblNameInstitution.setText("name_Institución");
    panelRound1.add(lblNameInstitution, new org.netbeans.lib.awtextra.AbsoluteConstraints(21, 201, 230, -1));

    lblDescrition.setForeground(new java.awt.Color(255, 255, 255));
    lblDescrition.setText("description_Institution");
    lblDescrition.setToolTipText("");
    lblDescrition.setVerticalAlignment(javax.swing.SwingConstants.TOP);
    panelRound1.add(lblDescrition, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 230, 130));

    add(panelRound1, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 140, 270, 460));
  }// </editor-fold>//GEN-END:initComponents

  private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnSearchActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnSearch;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JLabel lblDescrition;
  private javax.swing.JLabel lblNameInstitution;
  private javax.swing.JLabel lblNameUser;
  private javax.swing.JLabel lblNumber;
  private schemes.PanelRound panelRound1;
  private javax.swing.JTable tblBookUser;
  private LIB.JTexfieldPH_FielTex txtSearch;
  // End of variables declaration//GEN-END:variables
}
