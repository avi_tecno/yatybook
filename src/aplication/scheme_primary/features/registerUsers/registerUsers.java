package aplication.scheme_primary.features.registerUsers;

public class registerUsers extends javax.swing.JPanel {

  public registerUsers() {
    initComponents();
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel1 = new javax.swing.JLabel();
    btnDelete = new javax.swing.JButton();
    btnSeacrh = new javax.swing.JButton();
    btnAdd = new javax.swing.JButton();
    jLabel2 = new javax.swing.JLabel();
    jTexfieldPH_FielTex1 = new LIB.JTexfieldPH_FielTex();
    btnEdit = new javax.swing.JButton();
    jScrollPane1 = new javax.swing.JScrollPane();
    jTable1 = new javax.swing.JTable();

    setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(47, 64, 79));
    jLabel1.setText("Registro de Usuarios");
    add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

    btnDelete.setBackground(new java.awt.Color(102, 0, 0));
    btnDelete.setForeground(new java.awt.Color(255, 255, 255));
    btnDelete.setText("Eliminar");
    btnDelete.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnDeleteActionPerformed(evt);
      }
    });
    add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 30, 90, 30));

    btnSeacrh.setBackground(new java.awt.Color(56, 148, 161));
    btnSeacrh.setForeground(new java.awt.Color(255, 255, 255));
    btnSeacrh.setText("Buscar");
    btnSeacrh.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnSeacrhActionPerformed(evt);
      }
    });
    add(btnSeacrh, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 90, 80, 30));

    btnAdd.setBackground(new java.awt.Color(47, 64, 79));
    btnAdd.setForeground(new java.awt.Color(255, 255, 255));
    btnAdd.setText("Agregar");
    add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 30, 80, 30));

    jLabel2.setText("Buscar:");
    add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, -1));

    jTexfieldPH_FielTex1.setPlaceholder("Buscar Usuario");
    add(jTexfieldPH_FielTex1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, 610, -1));

    btnEdit.setBackground(new java.awt.Color(56, 148, 161));
    btnEdit.setForeground(new java.awt.Color(255, 255, 255));
    btnEdit.setText("Editar");
    add(btnEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(611, 30, 80, 30));

    jTable1.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null, null},
        {null, null, null, null, null},
        {null, null, null, null, null},
        {null, null, null, null, null}
      },
      new String [] {
        "Código", "Nombres", "Apellidos", "Nivel", "Curso/Grado"
      }
    ));
    jScrollPane1.setViewportView(jTable1);

    add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, 760, 490));
  }// </editor-fold>//GEN-END:initComponents

  private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnDeleteActionPerformed

  private void btnSeacrhActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeacrhActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_btnSeacrhActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnAdd;
  private javax.swing.JButton btnDelete;
  private javax.swing.JButton btnEdit;
  private javax.swing.JButton btnSeacrh;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable jTable1;
  private LIB.JTexfieldPH_FielTex jTexfieldPH_FielTex1;
  // End of variables declaration//GEN-END:variables
}
