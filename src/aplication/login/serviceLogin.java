package aplication.login;

import aplication.scheme_primary.schemeMain;
import connection.connection;
import connection.model.user;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class serviceLogin {
  
  user usr = new user();
  
  public void validationUser(JTextField code, JPasswordField password, int category, boolean action) {
    try {
      ResultSet rs = null;
      PreparedStatement ps = null;
      connection connect = new connection();
      String querySelect = "SELECT*FROM teacher WHERE id_teacher=(?) AND password_teacher=(?) AND teacher.id_category=(?);";
      ps = connect.getConnection().prepareStatement(querySelect);
      String pass = String.valueOf(password.getPassword());
      ps.setString(1, code.getText());
      ps.setString(2, pass);
      ps.setInt(3, category);
      rs = ps.executeQuery();
      if (rs.next()) {
        userByID(rs.getNString("id_teacher"), rs.getNString("password_teacher"), rs.getNString("name_teacher"), rs.getNString("lastname_teacher"), rs.getNString("location_teacher"), rs.getNString("phone_teacher"), rs.getNString("mail_teacher"), rs.getInt("id_level"), rs.getInt("id_grade"), rs.getInt("id_category"), rs.getInt("id_school"));
        action = true;
        connect.desconnection();
      } else {
        JOptionPane.showMessageDialog(null, "Este Usuario no se encuentra Registrado, Vuelve a Intentarlo.");
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public void userByID(String id, String password, String name, String lastname, String location, String phone, String mail, int id_level, int id_grade, int id_category, int id_school) {
    usr.setId_teacher(id);
    usr.setPassword_teacher(password);
    usr.setName_teacher(name);
    usr.setLastname_teacher(lastname);
    usr.setLocation_teacher(location);
    usr.setPhone_teacher(phone);
    usr.setMail_teacher(mail);
    usr.setId_level(id_level);
    usr.setId_grade(id_grade);
    usr.setId_category(id_category);
    usr.setId_school(id_school);
    
    login log = new login();
    
    schemeMain schemeObject = new schemeMain(usr);
    schemeObject.setVisible(true);
    log.dispose();
  }
}
