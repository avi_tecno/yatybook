package aplication.login;

import com.formdev.flatlaf.FlatLightLaf;
import connection.connection;
import java.awt.Image;
import java.awt.Toolkit;

public class login extends javax.swing.JFrame {

  connection connect;
  public login() {
    connect = new connection();
    this.setUndecorated(true);
    initComponents();
    this.setLocationRelativeTo(null);
    this.setTitle("Yaty Book 1.0");
    this.setResizable(false);
    this.setSize(825, 550);
    setIconImage(getIconImage());
    textCode.requestFocus();
    connect.getConnection();
  }
  
  public int initPropise() {
    String typeUser = (String) comboType.getSelectedItem();
    int typeCategory;
    if (typeUser.equals("Administrador")) {
      typeCategory = 1; // Use "ADMIN"
    } else {
      typeCategory = 2; // Use "TEACHER"
    }
    return typeCategory;
  }
  
  @Override
  public Image getIconImage(){
    Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icons/Icon-YB.png"));
    return retValue;
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    roboto1 = new efectos.Roboto();
    roboto2 = new efectos.Roboto();
    panelRound1 = new schemes.PanelRound();
    panelImage1 = new org.edisoncor.gui.panel.PanelImage();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    jPanel1 = new javax.swing.JPanel();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    jLabel6 = new javax.swing.JLabel();
    textPassword = new LIB.JTexfieldPH_Password();
    textCode = new LIB.JTexfieldPH_FielTex();
    jLabel7 = new javax.swing.JLabel();
    comboType = new javax.swing.JComboBox<>();
    btnInitSetion = new javax.swing.JButton();
    btnClose = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setBackground(new java.awt.Color(240, 241, 238));
    getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panelRound1.setBackground(new java.awt.Color(47, 64, 79));
    panelRound1.setPreferredSize(new java.awt.Dimension(330, 500));
    panelRound1.setRoundBottomLeft(10);
    panelRound1.setRoundBottomRight(10);
    panelRound1.setRoundTopLeft(10);
    panelRound1.setRoundTopRight(10);
    panelRound1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N

    javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
    panelImage1.setLayout(panelImage1Layout);
    panelImage1Layout.setHorizontalGroup(
      panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 200, Short.MAX_VALUE)
    );
    panelImage1Layout.setVerticalGroup(
      panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 186, Short.MAX_VALUE)
    );

    panelRound1.add(panelImage1, new org.netbeans.lib.awtextra.AbsoluteConstraints(69, 132, -1, -1));

    jLabel1.setForeground(new java.awt.Color(255, 255, 255));
    jLabel1.setText("Somos tu Servicio de Biblioteca de Confianza; estamos pre-");
    jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
    jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
    panelRound1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 433, 318, -1));

    jLabel2.setForeground(new java.awt.Color(255, 255, 255));
    jLabel2.setText("parados para dar solución a tú Administración de Biblioteca");
    jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
    jLabel2.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
    panelRound1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 455, -1, -1));

    jLabel3.setForeground(new java.awt.Color(255, 255, 255));
    jLabel3.setText("Escolar.");
    jLabel3.setVerticalAlignment(javax.swing.SwingConstants.TOP);
    jLabel3.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
    panelRound1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 477, 251, -1));

    getContentPane().add(panelRound1, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 14, 350, 522));

    jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
    jLabel4.setForeground(new java.awt.Color(47, 64, 79));
    jLabel4.setText("Iniciar Sesión");
    jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(61, 56, -1, -1));

    jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    jLabel5.setText("Tipo de Usuario:");
    jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(61, 112, -1, -1));

    jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    jLabel6.setText("Código:");
    jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(61, 200, -1, -1));

    textPassword.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    textPassword.setPlaceholder("Contraseña");
    textPassword.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        textPasswordActionPerformed(evt);
      }
    });
    jPanel1.add(textPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 320, 245, 38));

    textCode.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    textCode.setPlaceholder("Código");
    textCode.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        textCodeActionPerformed(evt);
      }
    });
    jPanel1.add(textCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(61, 238, 245, 38));

    jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    jLabel7.setText("Contraseña:");
    jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(61, 288, -1, -1));

    comboType.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
    comboType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Administrador", "Docente" }));
    comboType.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        comboTypeActionPerformed(evt);
      }
    });
    jPanel1.add(comboType, new org.netbeans.lib.awtextra.AbsoluteConstraints(61, 150, 245, 38));

    btnInitSetion.setBackground(new java.awt.Color(56, 148, 161));
    btnInitSetion.setFont(new java.awt.Font("Segoe UI", 1, 13)); // NOI18N
    btnInitSetion.setForeground(new java.awt.Color(255, 255, 255));
    btnInitSetion.setText("Iniciar Sesión");
    btnInitSetion.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnInitSetionActionPerformed(evt);
      }
    });
    jPanel1.add(btnInitSetion, new org.netbeans.lib.awtextra.AbsoluteConstraints(85, 398, 200, 43));

    btnClose.setBackground(new java.awt.Color(204, 0, 0));
    btnClose.setFont(new java.awt.Font("Segoe UI", 1, 13)); // NOI18N
    btnClose.setForeground(new java.awt.Color(255, 255, 255));
    btnClose.setText("Cerrar YatyBook");
    btnClose.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnCloseActionPerformed(evt);
      }
    });
    jPanel1.add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(85, 453, 200, 46));

    getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 0, 370, 550));

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void textCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textCodeActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_textCodeActionPerformed

  private void btnInitSetionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInitSetionActionPerformed
    serviceLogin service = new serviceLogin();
    int category = initPropise();
    boolean action = false;
    service.validationUser(textCode, textPassword, category, action);
    this.setVisible(action);
  }//GEN-LAST:event_btnInitSetionActionPerformed

  private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
    dispose();
    connect.desconnection();
    System.exit(0);
  }//GEN-LAST:event_btnCloseActionPerformed

  private void comboTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTypeActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_comboTypeActionPerformed

  private void textPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPasswordActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_textPasswordActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    
    try {
      FlatLightLaf.setup();
    } catch (Exception e) {
      e.printStackTrace();
    }

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new login().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  public javax.swing.JButton btnClose;
  public javax.swing.JButton btnInitSetion;
  private javax.swing.JComboBox<String> comboType;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JPanel jPanel1;
  private org.edisoncor.gui.panel.PanelImage panelImage1;
  private schemes.PanelRound panelRound1;
  private efectos.Roboto roboto1;
  private efectos.Roboto roboto2;
  public LIB.JTexfieldPH_FielTex textCode;
  public LIB.JTexfieldPH_Password textPassword;
  // End of variables declaration//GEN-END:variables
}
