package aplication.init;

import aplication.login.login;
import com.formdev.flatlaf.FlatLightLaf;
import connection.connection;
import java.awt.Image;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;

public class init extends javax.swing.JFrame {

  public init() {
    this.setLocationRelativeTo(null);
    this.setUndecorated(true);
    setIconImage(getIconImage());
    initComponents();
    this.setLocationRelativeTo(null);
    this.setTitle("Yaty Book 1.0");
    this.setResizable(false);
    this.setSize(730, 520);
    Shape form = new RoundRectangle2D.Double(0,0,this.getBounds().width,this.getBounds().height,28,28);
    this.setShape(form);
    Thread timer = new Thread(() -> {
      for (int index = 0; index <= 100; index ++) {
        try {
          progressInitChar.setValue(index);
          Thread.sleep(60);
          if (index == 100) {
            login log1 = new login();
            log1.setVisible(true);
            setVisible(false);
          }
        }catch (InterruptedException ex) {          
          Logger.getLogger(init.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    });
    timer.start();
  }
  
  @Override
  public Image getIconImage(){
    Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icons/Icon-YB.png"));
    return retValue;
  }
  
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelRound1 = new schemes.PanelRound();
    panelImage2 = new org.edisoncor.gui.panel.PanelImage();
    titleBar1 = new org.edisoncor.gui.varios.TitleBar();
    panelImage1 = new org.edisoncor.gui.panel.PanelImage();
    progressInitChar = new javax.swing.JProgressBar();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setBackground(new java.awt.Color(126, 126, 126));
    setUndecorated(true);
    setResizable(false);
    getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panelRound1.setBackground(new java.awt.Color(47, 64, 79));
    panelRound1.setRoundBottomLeft(10);
    panelRound1.setRoundBottomRight(10);
    panelRound1.setRoundTopLeft(10);
    panelRound1.setRoundTopRight(10);

    panelImage2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N

    javax.swing.GroupLayout panelImage2Layout = new javax.swing.GroupLayout(panelImage2);
    panelImage2.setLayout(panelImage2Layout);
    panelImage2Layout.setHorizontalGroup(
      panelImage2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 180, Short.MAX_VALUE)
    );
    panelImage2Layout.setVerticalGroup(
      panelImage2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 174, Short.MAX_VALUE)
    );

    javax.swing.GroupLayout panelRound1Layout = new javax.swing.GroupLayout(panelRound1);
    panelRound1.setLayout(panelRound1Layout);
    panelRound1Layout.setHorizontalGroup(
      panelRound1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelRound1Layout.createSequentialGroup()
        .addContainerGap(73, Short.MAX_VALUE)
        .addComponent(panelImage2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(67, 67, 67))
    );
    panelRound1Layout.setVerticalGroup(
      panelRound1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(panelRound1Layout.createSequentialGroup()
        .addGap(130, 130, 130)
        .addComponent(panelImage2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(146, Short.MAX_VALUE))
    );

    getContentPane().add(panelRound1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 24, -1, -1));
    getContentPane().add(titleBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(712, 190, -1, -1));

    panelImage1.setBackground(new java.awt.Color(0, 0, 0));
    panelImage1.setForeground(new java.awt.Color(0, 0, 0));
    panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/imageInitComponent.jpg"))); // NOI18N

    javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
    panelImage1.setLayout(panelImage1Layout);
    panelImage1Layout.setHorizontalGroup(
      panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 324, Short.MAX_VALUE)
    );
    panelImage1Layout.setVerticalGroup(
      panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 450, Short.MAX_VALUE)
    );

    getContentPane().add(panelImage1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 24, -1, -1));

    progressInitChar.setForeground(new java.awt.Color(0, 102, 102));
    progressInitChar.setPreferredSize(new java.awt.Dimension(146, 2));
    getContentPane().add(progressInitChar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 490, 670, 5));

    pack();
  }// </editor-fold>//GEN-END:initComponents

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    
    try {
      FlatLightLaf.setup();
    } catch (Exception e) {
      e.printStackTrace();
    }

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new init().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private org.edisoncor.gui.panel.PanelImage panelImage1;
  private org.edisoncor.gui.panel.PanelImage panelImage2;
  private schemes.PanelRound panelRound1;
  private javax.swing.JProgressBar progressInitChar;
  private org.edisoncor.gui.varios.TitleBar titleBar1;
  // End of variables declaration//GEN-END:variables

  public short add(int i, int i0) {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }
}
