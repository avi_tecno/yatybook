package connection.model;

import java.awt.Image;
import java.sql.Blob;

public class user {
  public String id_teacher;
  public String password_teacher;
  public String name_teacher;
  public String lastname_teacher;
  public String location_teacher;
  public String phone_teacher;
  public String mail_teacher;
  public Blob image_teacher;
  public int id_level;
  public int id_grade;
  public int id_category;
  public int id_school;
  
  public String getId_teacher() {
    return id_teacher;
  }

  public void setId_teacher(String id_teacher) {
    this.id_teacher = id_teacher;
  }

  public String getPassword_teacher() {
    return password_teacher;
  }

  public void setPassword_teacher(String password_teacher) {
    this.password_teacher = password_teacher;
  }

  public String getName_teacher() {
    return name_teacher;
  }

  public void setName_teacher(String name_teacher) {
    this.name_teacher = name_teacher;
  }

  public String getLastname_teacher() {
    return lastname_teacher;
  }

  public void setLastname_teacher(String lastname_teacher) {
    this.lastname_teacher = lastname_teacher;
  }

  public String getLocation_teacher() {
    return location_teacher;
  }

  public void setLocation_teacher(String location_teacher) {
    this.location_teacher = location_teacher;
  }

  public String getPhone_teacher() {
    return phone_teacher;
  }

  public void setPhone_teacher(String phone_teacher) {
    this.phone_teacher = phone_teacher;
  }

  public String getMail_teacher() {
    return mail_teacher;
  }

  public void setMail_teacher(String mail_teacher) {
    this.mail_teacher = mail_teacher;
  }

  public Blob getImage_teacher() {
    return image_teacher;
  }

  public void setImage_teacher(Blob image_teacher) {
    this.image_teacher = image_teacher;
  }

  public int getId_level() {
    return id_level;
  }

  public void setId_level(int id_level) {
    this.id_level = id_level;
  }

  public int getId_grade() {
    return id_grade;
  }

  public void setId_grade(int id_grade) {
    this.id_grade = id_grade;
  }

  public int getId_category() {
    return id_category;
  }

  public void setId_category(int id_category) {
    this.id_category = id_category;
  }

  public int getId_school() {
    return id_school;
  }

  public void setId_school(int id_school) {
    this.id_school = id_school;
  }
  
}