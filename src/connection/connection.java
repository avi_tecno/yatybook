package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class connection {
  private static Connection connect;
  private static final String driver = "com.mysql.jdbc.Driver";
  private static final String user = "root";
  private static final String password = "";
  private static final String url = "jdbc:mysql://localhost:3306/db_yati_book";

  public connection() {
    connect = null;
    try {
      Class.forName(driver);
      connect = DriverManager.getConnection(url,user, password);
      if (connect != null) {
        System.out.println("Connection START...");
      }
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("ERROR to  Connect ... " + e);
    }
  }
  // This Method Return Connect
  public Connection getConnection(){
    return connect;
  }
  
  public void desconnection() {
    connect = null;
    if (connect == null) {
      System.out.println("Connection STOP ...");
    }
  }
}
